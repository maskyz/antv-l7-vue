import HeatmapLayer from './LayerComponents/HeatmapLayer.vue';
import PointLayer from './LayerComponents/PointLayer.vue';
export {
  PointLayer,
  HeatmapLayer,
}